﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.WindowsAzure.MobileServices;
using TaksiShared.Model;

namespace TaksiShared.Service
{
    public class getNearUsers
    {
        MobileServiceClient client;
        IMobileServiceTable<Student> user;

        public getNearUsers()
        {
            client = new MobileServiceClient("http://taksiapp1.azurewebsites.net");
            user = client.GetTable<Student>();
        }

        public Student IsNear(Student user, Student other)
        {
            var lat = ((user.latitude - other.latitude) > 0) ? (user.latitude - other.latitude) : - (user.latitude - other.latitude);
            var lon = ((user.longitude - other.longitude) > 0) ? (user.longitude - other.longitude) : -(user.longitude - other.longitude);
            if (lat < 0.5 && lon < 0.5)
            {
                return user;
            }
        }
    }
}