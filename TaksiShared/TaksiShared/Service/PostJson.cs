﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace TaksiShared.Service
{
    public class PostJson
    {
        public void send()
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://taksiapp1.azurewebsites.net/UserInfo");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{\r\n  \"id\": \"string\",\r\n  \"createdAt\": \"2017-08-16T12:05:39.919Z\",\r\n  \"updatedAt\": \"2017-08-16T12:05:39.919Z\",\r\n  \"version\": \"string\",\r\n  \"deleted\": false\r\n}";
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
            }
        }

    }
}
