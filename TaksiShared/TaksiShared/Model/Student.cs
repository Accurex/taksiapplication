﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaksiShared.Model
{
    public class Student
    {
        public string id { get; set; }
        public string emailaddr { get; set; }
        public string password { get; set; }
        public string username { get; set; }
        public float longitude { get; set; }
        public float latitude { get; set; }
    }
}
