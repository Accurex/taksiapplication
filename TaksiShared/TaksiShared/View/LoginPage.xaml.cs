﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaksiShared.Model;
using TaksiShared.Service;
using TaksiShared.View;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Microsoft.WindowsAzure.MobileServices;

namespace TaksiShared.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
        ApiManager apim;

        public LoginPage ()
		{
			InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            apim = new ApiManager();
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            //Student stn = new Student();
            //stn.username = Entry_Username.Text;
            //stn.password = Entry_Password.Text;
            //var success = apim.LoginQuery(stn, stn.username, stn.password);
            //if (success)
            //{
            //    Navigation.PushAsync(new QuickStartGuide());
            //}
            //else
            //{
            //    DisplayAlert("Hatalı Giriş", "Doğru bilgileri girdiğinizden emin olun.", "Tamam");
            //}
            Navigation.PushAsync(new Map());
        }

        //private void AccessSuccess(string username, string pwd)
        //{
        //    Student loginCred = new Student();
        //    loginCred.emailaddr = username;
        //    loginCred.password = pwd;
        //}
    }
}