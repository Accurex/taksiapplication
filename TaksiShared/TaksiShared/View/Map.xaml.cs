﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Forms.Maps;

namespace TaksiShared.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Map : ContentPage
	{
        Geocoder geoCoder;
        Pin pin;
        //SearchBar Bar = new SearchBar();

        public Map ()
		{
			InitializeComponent ();
            geoCoder = new Geocoder();
            //var pin = new Pin()
            //{
            //    Position = new Position(37, -122),
            //    Label = "Some Pin!"
            //};
            //MyMap.Pins.Add(pin);
            //pin.Clicked += OnPinClicked; 
        }



        public async void CoordinateSearch()
        {
            var address = Bar.Text;
            var approximateLocations = await geoCoder.GetPositionsForAddressAsync(address);
            foreach (var position in approximateLocations)
            {
                if (position.Longitude > 27.969003 && position.Longitude < 29.413945 && position.Latitude < 41.306069 && position.Latitude > 40.815502)
                {
                    pin = new Pin()
                    {
                        Position = new Position(position.Latitude, position.Longitude),
                        Label = address,
                    };
                    pin.Clicked += (object sender, EventArgs e) =>
                    {
                        DisplayAlert(":)", ":)", ":)");
                    };
                    MyMap.Pins.Add(pin);
                    MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(position.Latitude, position.Longitude), Distance.FromMiles(1)));
                }
            }
        }

        private void SearchBar_SearchButtonPressed(object sender, EventArgs e)
        {
            CoordinateSearch();
            //var pin = new Pin()
            //{
            //    Position = new Position(41.054731, 29.027073),
            //    Label = "Ortaköy Mahallesi"
            //};
            //MyMap.Pins.Add(pin);
            //MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(41.054731, 29.027073), Distance.FromMiles(1)));
        }
    }
}