﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using TaksiShared.Model;
using TaksiShared.Service;

namespace TaksiShared.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class QuickStartGuide : CarouselPage
    {
        //ApiManager apim;
        public QuickStartGuide()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            //apim = new ApiManager();
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new LoginPage());
            //FillData();
        }

        //private void FillData()
        //{
        //    apim.getAll();
        //}
    }
}